#!/bin/sh
echo "BRANCH_NAME = "$BRANCH_NAME
echo "GIT_COMMIT = "$GIT_COMMIT
echo "GIT_BRANCH = "$GIT_BRANCH
echo "GIT_LOCAL_BRANCH = "$GIT_LOCAL_BRANCH
echo "GIT_PREVIOUS_COMMIT = "$GIT_PREVIOUS_COMMIT
$GIT_BRANCH \| grep -oP '(?<=origin/feature\\)\w+'

echo "${GIT_BRANCH#*\\}"

JIRA_BRANCH=`git rev-parse HEAD | git branch -a --contains | grep remotes | sed s/.*remotes.origin.//`
echo "JIRA_BRANCH" :: $JIRA_BRANCH
echo "BUILD_NUMBER" :: $BUILD_NUMBER
echo "BUILD_ID" :: $BUILD_ID
echo "BUILD_DISPLAY_NAME" :: $BUILD_DISPLAY_NAME
echo "JOB_NAME" :: $JOB_NAME
echo "JOB_BASE_NAME" :: $JOB_BASE_NAME
echo "BUILD_TAG" :: $BUILD_TAG
echo "EXECUTOR_NUMBER" :: $EXECUTOR_NUMBER
echo "NODE_NAME" :: $NODE_NAME
echo "NODE_LABELS" :: $NODE_LABELS
echo "WORKSPACE" :: $WORKSPACE
echo "JENKINS_HOME" :: $JENKINS_HOME
echo "JENKINS_URL" :: $JENKINS_URL
echo "BUILD_URL" ::$BUILD_URL
echo "JOB_URL" :: $JOB_URL
echo "JIRA_ISSUES" :: $JIRA_ISSUES
echo "JIRA_URL" :: $JIRA_URL



echo @@@@@@@@@@@@@@@@@@@@@@@@@

git diff --name-only $GIT_PREVIOUS_COMMIT $GIT_COMMIT > input.txt ### Add commit id as prefix of input.txt and all temporary
cat input.txt
set -e

# Declare the required Global Variables
deployFileExt=bteq
baseDir=~/Repos/eim_ssot-Copy
#deployFolder=$baseDir/filesToDeploy
 deployFolder=$WORKSPACE/filesToDeploy
# Create Directories to copy the .bteq files that needs to be deployed
mkdir -p $deployFolder/{Macros,Views,Tables} ###has to be changed to match git directory structure
touch filesdeployed.txt;rm filesdeployed.txt;touch filesdeployed.txt
touch files_to_be_removed.txt;rm files_to_be_removed.txt;touch files_to_be_removed.txt

# Create input.txt from the list of files that changed from the previous deployment
# git diff command goes here to create the input.txt

copyFilesToDeployFolder(){

# $1 = input.txt

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Copying files to deploy folder
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found. Continue with next ..";  }

while IFS= read -r file
do

if [ -f "$file" ]; then
	echo "Copying file $file to 'filesToDeploy' folder"

    cp --parents $file $deployFolder ### copying with directory structure
fi	
		
done < "${in}"
}

copyFilesToDeployFolder input.txt

CreateInputFileFromDeployFolder(){
in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found."; exit 1; }

awk -v prefix="filesToDeploy/" '{print prefix $0}' input.txt > inputFileFromDeployFolder.txt

}

CreateInputFileFromDeployFolder input.txt

 

#Create seperate list of Tables/Views/Macros that needs to be deployed from inputFileFromDeployFolder.txt
in="${1:-inputFileFromDeployFolder.txt}"
echo $in
echo $WORKSPACE
if [ -f "listOfMacrosToDeploy.txt" ]; then
rm listOfMacrosToDeploy.txt
fi
if [ -f "listOfTablesToDeploy.txt" ]; then
rm listOfTablesToDeploy.txt
fi
if [ -f "listOfViewsToDeploy.txt" ]; then
rm listOfViewsToDeploy.txt
fi

sed -i $'s/\r$//' ${in}

[ ! -f "$in" ] && { echo "$0 - Line $in not found."; exit 1; }

while IFS= read -r line
do
  if [ -f "$WORKSPACE/$line" ]; then
    if [[ $line == *"BTCH"* ]]; then
    echo $line >> listOfMacrosToDeploy.txt
	echo $line
    fi
    if [[ $line == *"TABLE"* ]]; then
    echo $line >> listOfTablesToDeploy.txt
	echo $line
    fi
    if [[ $line == *"VIEW"* ]]; then
    echo $line >> listOfViewsToDeploy.txt
	echo $line
    fi
  fi
done < "${in}"

#echo "filesToDeploy/Tables/EMPL.sql" > listOfTablesToDeploy.txt

if [ -f "listOfMacrosToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo List of Macros to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  cat listOfMacrosToDeploy.txt
fi

if [ -f "listOfTablesToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo List of Tables to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  cat listOfTablesToDeploy.txt
fi

if [ -f "listOfViewsToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo List of Views to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  cat listOfViewsToDeploy.txt
fi

tokenizeLowerEnv(){

# $1 == inputFileFromDeployFolder.txt
# $2 = _BETA And Empty for Prod

# local BRANCH_NAME="master"
echo $BRANCH_NAME > BRANCH.TXT
  if [[ ! $BRANCH_NAME =~ ^(int|mater)$ ]]; then
    local envToken="_BETA" 
  fi
  if [[ $BRANCH_NAME == "int" ]]; then
    local envToken="_TEST" 
  fi
  if [[ $BRANCH_NAME == "master" ]]; then
    local envToken="" 
  fi

  in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found."; exit 1; }

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Creating Tokenized scripts
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
while IFS= read -r file
do
echo Converting  "$file" to the $2 Script
echo $'\n' >> $file 
cat $file | sed -n 's/.*\(EP_SSOT.*[.]\)/\1/p' > changes.txt 

in="${2:-changes.txt}"
        [ ! -f "$in" ] && { echo "$0 - File $in not found."; exit 1; }
            while IFS= read -r line
                do
                
                echo "file is " :: $file
                echo "line is " :: $line
                
                echo $line > line.txt
                              
                #local replaceString=$(echo "${line}")
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                echo "replaceStirng" :: $replaceString
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                schema="$( cut -d '.' -f 1 <<< "$line" )"
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo "schema name is " :: $schema
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                
                #local replacedString="$(sed s/$schema/"${schema}$envToken"/g line.txt)"
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo "replacedStirng" :: $replacedString
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                #sed -i "s/${replaceString}/${replacedString}/g" $file
                sed -i "s/$schema/${schema}${envToken}/g" $file 				
                #sed -i "0,/${replaceString//\//\\/}/s//${replacedString//\//\\/}/" $file 
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                cat $file 
                echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            done < "${in}"
  
  
done < "${in}"
}


if [ -f "listOfTablesToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Tokenize  Tables to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  tokenizeLowerEnv listOfTablesToDeploy.txt
fi

if [ -f "listOfViewsToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Tokenize Views to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  tokenizeLowerEnv listOfViewsToDeploy.txt
fi

if [ -f "listOfMacrosToDeploy.txt" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Tokenize Tables to deploy
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if [[ ! $BRANCH_NAME =~ ^(master)$ ]]; then
 tokenizeLowerEnv listOfMacrosToDeploy.txt
fi 
fi


createBTEQ(){
# $1 = listOfTablesToDeploy.txt/listOfViewsToDeploy.txt/listOfMacrosToDeploy.txt
# $2 = Tables/Views/Macros
if [ -f "$1" ]; then
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Creating bteq scripts - $	 
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found. Continue with next ..";  }

while IFS= read -r file
do
	echo "Converting $file to bteq script"
    	
	local filePATH=$(echo "${file%/*}") # Populate PATH of the file into filePATH Variable
	local fileNameWithExt=$(echo $file | tr '/' '_' | sed 's/^[^_]*_//g') # Populate Filename with extension into fileName Variable
	local fileName=$(echo "${fileNameWithExt%.*}") # Populate Filename only into fileNameWithOutEXT Variable
    
	echo .SET WIDTH 1000 > $deployFolder/$2/$fileName.$deployFileExt
    echo .LOGON 192.168.1.101/dbc, Sloth#551 >> $deployFolder/$2/$fileName.$deployFileExt
    cat $file >> $deployFolder/$2/$fileName.$deployFileExt
    echo $'\n' >> $deployFolder/$2/$fileName.$deployFileExt
    echo ".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE" >> $deployFolder/$2/$fileName.$deployFileExt
    echo ".EXIT ERRORCODE"   >> $deployFolder/$2/$fileName.$deployFileExt

done < "${in}"
fi
}

createBTEQ listOfTablesToDeploy.txt Tables 
createBTEQ listOfViewsToDeploy.txt Views
createBTEQ listOfMacrosToDeploy.txt Macros

checkObjectExists(){
# $1 = $file from CreateBackupScripts
# $2 = T/V/M
# $3 = _BETA

      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      echo "In Check Object Exists dollar 1 is $1 and dollar 2 is $2 and dollar 3 is $3" 
      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

 filePath=$(echo "${file%/*}")
 fileParentDir=$(echo `basename $filePath`)
 fileNameWithExt=$(echo `basename $file`)
 fileName=$(echo "${fileNameWithExt%.*}")
    
    echo .SET WIDTH 1000 > $2_file.bteq
    echo .LOGON 192.168.1.101/dbc, Sloth#551 >> $2_file.bteq	
    echo SELECT databasename, tablename >> $2_file.bteq
    echo FROM dbc.tables >> $2_file.bteq
    echo WHERE databasename = $(echo \'$fileParentDir$3\') >> $2_file.bteq
    echo    AND tablename = $(echo \'$fileName\') >> $2_file.bteq
    echo    AND tablekind = $(echo \'$2\'\;) >> $2_file.bteq
    # echo $'\n' >> $2_file.bteq
    echo .IF ERRORCODE \<\> 0 THEN .EXIT ERRORCODE >> $2_file.bteq
    # echo .IF ACTIVITYCOUNT = 0 THEN .EXIT 255 >> $2_file.bteq

    
   # RUN THE BTEQ, CHECK THE ERROR CODE, PASS THE EXISTS OR NOT EXISTS RESPONSE TO THE CALLER METHOD
    bteq < $2_file.bteq > $2_file.log 

      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      echo "Checking 'One row found' is there in log file" 
      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      

     if grep -q  'One row found' $2_file.log; then
       abc=123
          echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          echo "Checking 'One row found' is there in log file from if success" 
          echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          echo abc 
     else
       abc=345
          echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          echo "Checking 'One row found' is there in log file from if failure" 
          echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          echo abc 
     fi

      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      echo "Result of the object exits check is abc after if" ::
      echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      # echo $abc 
      echo $abc > result.txt
#rm file.log
#echo 123
}


createDropObjectScripts(){
# $1 = listOfTablesToDeploy.txt/listOfViewsToDeploy.txt/listOfMacrosToDeploy.txt
# $2 = Tables/Views/Macros
# $3 = Table/View/Macro
# $4 = _BETA And Empty for Prod - This should be redundant now.

  if [[ ! $BRANCH_NAME =~ ^(int|mater)$ ]]; then
    local envToken="_BETA" 
  fi
  if [[ $BRANCH_NAME == "int" ]]; then
    local envToken="_TEST" 
  fi
  if [[ $BRANCH_NAME == "master" ]]; then
    local envToken="" 
  fi

if [ -f "$1" ]; then
  in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found. Continue with next ..";  }

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Creating Drop Object scripts - $2 
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
i=1
while IFS= read -r file
do
echo Adding $3 "$file" to the Drop Object Script
 filePath=$(echo "${file%/*}")
 fileParentDir=$(echo `basename $filePath`)
 fileNameWithExt=$(echo `basename $file`)
 fileName=$(echo "${fileNameWithExt%.*}")
 echo DROP $3 $fileParentDir$envToken.$fileName; >> $deployFolder/$2/drop_$2.sh
 
 i=`expr $i + 1`
done < "${in}"
fi
}

createBackupAndRollbackScripts(){
# $1 = $file
# $2 = Tables/Views/Macros
# $3 = Table/View/Macro
# $4 = $i 
# $5 = _BETA And Empty for Prod -- Made changes this should be redundant now.


  if [[ ! $BRANCH_NAME =~ ^(int|mater)$ ]]; then
    local envToken="_BETA" 
  fi
  if [[ $BRANCH_NAME == "int" ]]; then
    local envToken="_TEST" 
  fi
  if [[ $BRANCH_NAME == "master" ]]; then
    local envToken="" 
  fi

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo envToken from create backup and rollback scripts is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $envToken

 filePath=$(echo "${file%/*}")
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo filePath from createBackupAndRollbackScripts section of the script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $filePath

 fileParentDir=$(echo `basename $filePath`)
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo fileParentDir from createBackupAndRollbackScripts section of the script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $fileParentDir


 fileNameWithExt=$(echo `basename $file`)
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo fileNameWithExt from createBackupAndRollbackScripts section of the script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $fileNameWithExt


 fileName=$(echo "${fileNameWithExt%.*}")
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo fileName from createBackupAndRollbackScripts section of the script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $fileName
 
 echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo COMMIT_MSG from demo script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $COMMIT_MSG

echo "$COMMIT_MSG" > commitMessageIntact.txt
cat commitMessageIntact.txt | egrep '...-[0-9]{1,4}' > jiraIssues_egrep.txt 
PRIMARY_JIRA_ISSUE=$(head -n 1 jiraIssues_egrep.txt)
 
 echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo PRIMARY_JIRA_ISSUE from demo script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $PRIMARY_JIRA_ISSUE

 branch=$(echo `basename $BRANCH_NAME`)
#  jira_id="$( cut -d '_' -f 1 <<< "$branch" )"

jira_id=$(head -n 1 jiraIssues_egrep.txt)
 #jira_2_id="$( cut -d '-' -f 2 <<< "$branch" )"
 #jira_id=$(echo "${jira_1_id}_${jira_2_id}")
 #JIRA_ISSUES=$(echo "${jira_1_id}-${jira_2_id}")
 echo jira_id is $jira_id 

 jira_id_1="$( cut -d '-' -f 1 <<< "$jira_id" )"
 echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo jira_id_1 from demo script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $jira_id_1

 jira_id_2="$( cut -d '-' -f 2 <<< "$jira_id" )"
 echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo jira_id_2 from demo script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $jira_id_2



 td_jn_id=$(echo "${jira_id_1}_${jira_id_2}")


echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo td_jn_id from demo script is ..
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo $td_jn_id

 #echo JIRA_ISSUES is $JIRA_ISSUES 
  if [[ $3 == "TABLE" ]]; then
  # checkTableExists $file 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo "in if statement of $3 == "TABLE"... from create backbup and rollback scripts next step is check if object exits"
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    


  # result=$(checkObjectExists $file T $envToken)

    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo result of object exists is ..
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo $result

    checkObjectExists $file T $envToken
results=$(cat result.txt)

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo results of object exists is ..
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo $results


    if [[ $results == 123 ]]; then
        echo Adding $3 "$file" to the Backup Script
        echo CREATE $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i AS $fileParentDir$envToken.$fileName with NO DATA and STATS';' >> $deployFolder/$2/backup_$2.sh
        echo DROP $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i';' >> $deployFolder/$2/drop_backup_$2.sh
        echo Adding $3 "$file" to the Rollback Script
        echo CREATE $3 $fileParentDir$envToken.$fileName AS $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i with NO DATA and STATS';' >> $deployFolder/$2/rollback_$2.sh
    	  # echo Adding $3 "$file" into BACKUP_TABLE_METADATA - This will maintain the Names of the Backup Table and its corresponding Backedup Table.
        echo INSERT INTO EP_SSOT_NTGT$envToken.BACKUP_TABLE_METADATA '('BACKUP_TABLE_NAME,ORIG_TABLE_NAME,JENKINS_BUILD_TAG')' VALUES '(' "'"JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i"'","'"$fileParentDir$envToken.$fileName"'","'"$BUILD_TAG"'" ')' ';' > meta_$3_$i.sh
                
        echo .SET WIDTH 1000 > meta_$3_$i.$deployFileExt
      	echo .LOGON 192.168.1.101/dbc, Sloth#551 >> meta_$3_$i.$deployFileExt
    	cat meta_$3_$i.sh >> meta_$3_$i.$deployFileExt
   	 	echo ".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE" >> meta_$3_$i.$deployFileExt
    	echo ".EXIT ERRORCODE"   >> meta_$3_$i.$deployFileExt

      echo Adding JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i, $fileParentDir$envToken.$fileName and $BUILD_TAG into BACKUP_TABLE_METADATA - This TABLE will maintain the  Jenkins Job Details, Names of the Backup TABLE and its corresponding Backedup TABLE. This information will be valuable incase of any issues identified at a later stage of the deployment.  
        bteq < meta_$3_$i.bteq > meta_$3_$i.log
    fi
  fi

  if [[ $3 == "VIEW" ]]; then
  # checkViewExists $file 
  results=$(checkObjectExists $file V $envToken)
    if [[ $result == 123 ]]; then
        echo Adding $3 "$file" to the Backup Script
        echo RENAME $3 $fileParentDir$envToken.$fileName TO $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i';'  >> $deployFolder/$2/backup_$2.sh
        echo DROP $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i';' >> $deployFolder/$2/drop_backup_$2.sh
        echo Adding $3 "$file" to the Rollback Script
        echo RENAME $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i TO $fileParentDir$envToken.$fileName';' >> $deployFolder/$2/rollback_$2.sh
    	
      echo INSERT INTO EP_SSOT_NTGT$envToken.BACKUP_VIEW_METADATA '('BACKUP_VIEW_NAME,ORIG_VIEW_NAME,JENKINS_BUILD_TAG')' VALUES '(' "'"JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i"'","'"$fileParentDir$5.$fileName"'","'"$BUILD_TAG"'" ')' ';' > meta_$3_$i.sh
                
        echo .SET WIDTH 1000 > meta_$3_$i.$deployFileExt
      	echo .LOGON 192.168.1.101/dbc, Sloth#551 >> meta_$3_$i.$deployFileExt
    	cat meta_$3_$i.sh >> meta_$3_$i.$deployFileExt
   	 	echo ".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE" >> meta_$3_$i.$deployFileExt
    	echo ".EXIT ERRORCODE"   >> meta_$3_$i.$deployFileExt

      echo Adding $3 "$file" into BACKUP_VIEW_METADATA - This will maintain the Names of the Backup VIEW and its corresponding Backedup VIEW.  
        bteq < meta_$3_$i.bteq > meta_$3_$i.log
    fi
  fi

  if [[ $3 == "MACRO" ]]; then
  # checkMacroExists $file 
  results=$(checkObjectExists $file M $envToken)
    if [[ $result == 123 ]]; then
        echo Adding $3 "$file" to the Backup Script
        echo RENAME $3 $fileParentDir$envToken.$fileName TO $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i';'  >> $deployFolder/$2/backup_$2.sh
        echo DROP $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i';' >> $deployFolder/$2/drop_backup_$2.sh
        echo Adding $3 "$file" to the Rollback Script
        echo RENAME $3 $fileParentDir$envToken.JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i  TO $fileParentDir$envToken.$fileName';' >> $deployFolder/$2/rollback_$2.sh
    	echo INSERT INTO EP_SSOT_NTGT$envToken.BACKUP_MACRO_METADATA '('BACKUP_MACRO_NAME,ORIG_MACRO_NAME,JENKINS_BUILD_TAG')' VALUES '(' "'"JN_BKP_JID_${td_jn_id}_${BUILD_NUMBER}_$3_$i"'","'"$fileParentDir$envToken.$fileName"'","'"$BUILD_TAG"'" ')' ';' > meta_$3_$i.sh
                 
        echo .SET WIDTH 1000 > meta_$3_$i.$deployFileExt
      	echo .LOGON 192.168.1.101/dbc, Sloth#551 >> meta_$3_$i.$deployFileExt
    	cat meta_$3_$i.sh >> meta_$3_$i.$deployFileExt
   	 	echo ".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE" >> meta_$3_$i.$deployFileExt
    	echo ".EXIT ERRORCODE"   >> meta_$3_$i.$deployFileExt
        
        echo Adding $3 "$file" into BACKUP_MACRO_METADATA - This will maintain the Names of the Backup MACRO and its corresponding Backedup MACRO.
        bteq < meta_$3_$i.bteq > meta_$3_$i.log  
    fi
  fi
}

convertScriptsToBteq(){
# $1 Tables/Views/Macros
# $2 backup/rollback/drop

echo Conveting $2 script to bteq format

if [ -f "$deployFolder/$1/$2_$1.sh" ]; then 
echo .SET WIDTH 1000 > $deployFolder/$1/$2_$1.$deployFileExt
echo .LOGON 192.168.1.101/dbc, Sloth#551 >> $deployFolder/$1/$2_$1.$deployFileExt
cat $deployFolder/$1/$2_$1.sh >> $deployFolder/$1/$2_$1.$deployFileExt
# echo " " >> $deployFolder/$1/$2_$1.$deployFileExt
echo $'\n' >> $deployFolder/$1/$2_$1.$deployFileExt
echo ".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE" >> $deployFolder/$1/$2_$1.$deployFileExt
echo ".EXIT ERRORCODE"   >> $deployFolder/$1/$2_$1.$deployFileExt
fi
}



callToScripts(){
# $1 = listOfTablesToDeploy.txt/listOfViewsToDeploy.txt/listOfMacrosToDeploy.txt
# $2 = Tables/Views/Macros
# $3 = Table/View/Macro
# $4 = Environment For Ex. _BETA. If empty it is PROD

if [ -f "$1" ]; then
  in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found. Continue with next ..";  }

i=1
while IFS= read -r file
do
 filePath=$(echo "${file%/*}")
 fileParentDir=$(echo `basename $filePath`)
 fileNameWithExt=$(echo `basename $file`)
 fileName=$(echo "${fileNameWithExt%.*}")
 
echo Calling createBackupAndRollbackScripts scripts with $file $2 $3 $i and $4
#  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#  echo 'Creating backup scripts if' $3  exists
#  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#createBackupAndRollbackScripts $file $2 $3 $i 

# Calling Rollback scripts
#  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#  echo 'Creating rollback scripts if' $3  exists
#  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#createRollbackScripts $file $2 $3 $i $4
 
i=`expr $i + 1`

done < "${in}"
fi

# Calling Drop Object scripts for All Tables/Views/Macros 
createDropObjectScripts $1 $2 $3 

# Calling Convert to Bteq scripts
  # $1 Tables/Views/Macros
  # $2 backup/rollback/drop

convertScriptsToBteq $2 backup
convertScriptsToBteq $2 rollback
convertScriptsToBteq $2 drop 
convertScriptsToBteq $2 drop_backup
}

callToScripts listOfTablesToDeploy.txt Tables TABLE  
callToScripts listOfViewsToDeploy.txt Views VIEW 
callToScripts listOfMacrosToDeploy.txt Macros MACRO 



deployProcess(){

  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  echo Backing up  - $2 'if exists'
  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

if [ -f "$deployFolder/$2/backup_$2.bteq" ]; then 
echo "The $deployFolder/$2/backup_$2.bteq exists.. taking backup of $2"; 
bteq < $deployFolder/$2/backup_$2.bteq > backup_$2.log
fi

#cd $deployFolder
if [ -f "$1" ]; then

  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  echo Deploying  - $2 
  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

in="${1:-$1}"
  [ ! -f "$in" ] && { echo "$0 - File $in not found. Continue with next ..";  }


i=1
while IFS= read -r file
do
  local filePATH=$(echo "${file%/*}") # Populate PATH of the file into filePATH Variable
	local fileNameWithExt=$(echo $file | tr '/' '_' | sed 's/^[^_]*_//g') # Populate Filename with extension into fileName Variable # Populate Filename with extension into fileName Variable
	local fileName=$(echo "${fileNameWithExt%.*}") # Populate Filename only into fileNameWithOutEXT Variable
  
  echo Deploying  - $2 $fileName
  diff filesdeployed.txt inputFileFromDeployFolder.txt|grep ">"|sed 's/> filesToDeploy/git rm $git_repo_cont/g' > files_to_be_removed.txt
    bteq < $deployFolder/$2/$fileName.$deployFileExt > $deployFolder/$2/$fileName.log

  echo $file >> filesdeployed.txt 
	

 i=`expr $i + 1`
done < "${in}"
fi

#  echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# echo Deleting Backups on Successful deploy of  - $2 
# echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#bteq < $deployFolder/$2/drop_backup_$2.bteq > $deployFolder/$2/drop_backup_$2.log
}

 deployProcess listOfTablesToDeploy.txt Tables
 deployProcess listOfViewsToDeploy.txt Views
 deployProcess listOfMacrosToDeploy.txt Macros
 echo "is it working"
 
