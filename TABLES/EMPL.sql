DROP TABLE CICD_Test.EMPL;

CREATE MULTISET TABLE CICD_Test.EMPL ,FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO,
     MAP = TD_MAP1
     (
      EMP_NAME VARCHAR(30) CHARACTER SET UNICODE NOT CASESPECIFIC NOT NULL,
      DEPT_NAME VARCHAR(30) CHARACTER SET UNICODE NOT CASESPECIFIC NOT NULL,
      SALARY VARCHAR(30) CHARACTER SET UNICODE NOT CASESPECIFIC NOT NULL)
PRIMARY INDEX ( EMP_NAME ,DEPT_NAME ,SALARY );