CREATE VIEW CICD_Test.EMPL_VIEW
AS 
LOCKING ROW FOR ACCESS
SELECT 
EMP_NAME,
DEPT_NAME,
SALARY
FROM CICD_Test.EMPL;

